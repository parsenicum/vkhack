const favicons = require('favicons')
const fs = require('fs')
const util = require('util')
const parse = require('himalaya').parse
let config = require('./head.json')

const save = util.promisify(fs.writeFile)

const head = {
  title: 'Mentor',
  meta: [
    { charset: 'utf-8' },
    { name: 'viewport', content: 'width=device-width, initial-scale=1' },
    { hid: 'description', name: 'description', content: 'Mentor' },
    { name: 'theme-color', content: '#2196F3' },
  ],
  link: [
    { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    {
      rel: 'stylesheet',
      href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
    }
  ],
  script: [
    { src: "https://www.gstatic.com/firebasejs/7.0.0/firebase-app.js" },
    { src: "https://www.gstatic.com/firebasejs/7.0.0/firebase-analytics.js" },
    { src: "https://www.gstatic.com/firebasejs/7.0.0/firebase-messaging.js" },
  ]
}

const source = './logo.png'
const headerFile = './head.json'

const configuration = {
  path: "/",                                // Path for overriding default icons path. `string`
  appName: null,                            // Your application's name. `string`
  appShortName: null,                       // Your application's short_name. `string`. Optional. If not set, appName will be used
  appDescription: null,                     // Your application's description. `string`
  developerName: null,                      // Your (or your developer's) name. `string`
  developerURL: null,                       // Your (or your developer's) URL. `string`
  dir: "auto",                              // Primary text direction for name, short_name, and description
  lang: "en-US",                            // Primary language for name and short_name
  background: "#fff",                       // Background colour for flattened icons. `string`
  theme_color: "#fff",                      // Theme color user for example in Android's task switcher. `string`
  appleStatusBarStyle: "black-translucent", // Style for Apple status bar: "black-translucent", "default", "black". `string`
  display: "standalone",                    // Preferred display mode: "fullscreen", "standalone", "minimal-ui" or "browser". `string`
  orientation: "any",                       // Default orientation: "any", "natural", "portrait" or "landscape". `string`
  scope: "/",                               // set of URLs that the browser considers within your app
  start_url: "/?homescreen=1",              // Start URL when launching the application from a device. `string`
  version: "1.0",                           // Your application's version string. `string`
  logging: false,                           // Print logs to console? `boolean`
  pixel_art: false,                         // Keeps pixels "sharp" when scaling up, for pixel art.  Only supported in offline mode.
  loadManifestWithCredentials: false,       // Browsers don't send cookies when fetching a manifest, enable this to fix that. `boolean`
  icons: {
      // Platform Options:
      // - offset - offset in percentage
      // - background:
      //   * false - use default
      //   * true - force use default, e.g. set background for Android icons
      //   * color - set background for the specified icons
      //   * mask - apply mask in order to create circle icon (applied by default for firefox). `boolean`
      //   * overlayGlow - apply glow effect after mask has been applied (applied by default for firefox). `boolean`
      //   * overlayShadow - apply drop shadow after mask has been applied .`boolean`
      //
      android: true,              // Create Android homescreen icon. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
      appleIcon: true,            // Create Apple touch icons. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
      appleStartup: true,         // Create Apple startup images. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
      coast: true,                // Create Opera Coast icon. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
      favicons: true,             // Create regular favicons. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
      firefox: true,              // Create Firefox OS icons. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
      windows: true,              // Create Windows 8 tile icons. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
      yandex: true                // Create Yandex browser icon. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
  }
}

const convertArrayToList = ( arr ) => {
  return [ {} , ...arr].reduce( ( list , { key , value } ) => ({ ...list , [key] : value }) )
}

favicons(source, configuration, async (error, { images , files, html }) => {
    if (error) {
        console.log(error.message)
        return
    }

    try {
      const all = [ images, files ]

      for ( const item of all ) {
        for ( let i = 0; i < item.length; i++ )
          await save( `./static/${item[i].name}` , item[i].contents )
      }


      let data = parse( html.join('\n') )

      data = data.filter( ({attributes}) => attributes ).map( ({ tagName, attributes}) => (
        [ tagName , convertArrayToList( attributes ) ]
      ))

      const result = {}

      data.forEach( ([ key , value ]) =>
        Array.isArray(result[key]) ?
          result[key].push(value) : result[key] = [value]
      )

      fs.writeFileSync( headerFile ,
        JSON.stringify({ ...head ,
          meta: [ ...head.meta , ...result.meta],
          link: [ ...head.link , ...result.link],
        } , null , '  ') , 'utf8' )

      console.log(
        config
      )

    } catch (e) {
      console.log(
        e
      )
    }
})
