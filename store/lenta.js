export const state = () => ({
  mentor: [
    // name
    // data
    // icon
    // type
    // read
    {
      // for line
      data: Date.now() - Date.now() / 200,
      name: "test",
      icon: "eco",
      read: false,

      // for card
      type: "event",
      id: 0
    },
    {
      // for line
      data: Date.now(),
      name: "test 2",
      icon: "eco",
      read: true,

      // for card
      type: "weather",
      id: 1
    }
  ],
  admin: [

  ],
  ps: [
    {
      type: "weather"
    }
  ]
})

export const mutations = {
  addmentor(state, obj) {
    state.mentor.push(obj)
  },
  read(state, {id , k }) {
    state.mentor = state.mentor.map(item =>
      item.id === id ? { ...item, read: true } : item
    )
  },
  setLenta(state, [dir, pushes]) {
    state[dir] = pushes.map(_ => ({
      ..._,
      data: _.createdAt,
      name: _.title,
      icon: "eco",
      read: _.status != "new",
      type: "weather",
      color: "wheat",
      text: _.content
    }));
  }
}
