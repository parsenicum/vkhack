export const state = () => ({
  username: "Name surname",
  avatar: "https://placehold.it/30x30"
})


export const mutations = {
  put( state , { name , avatar }) {
    state.username = name
    state.avatar = avatar
  }
}
