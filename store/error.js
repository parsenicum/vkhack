export const state = () => ({
    isError: false,
    errorText: ''
})

export const mutations = {
    set(state, isError) {
        state.isError = isError;
    },
    show(state, errorText) {
        state.errorText = errorText;
        state.isError = true;
    }
}