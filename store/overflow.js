export const state = () => ({
  k: "",
  index: -1
})


export const mutations = {
  set(state , index ) {
    state.index = index
  },
  clear(state){
    state.index = -1
  }
}
