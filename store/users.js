const names = [
	"Абакум",
	"Абрам",
	"Абросим",
	"Аввакум",
	"Август",
	"Авдей",
	"Авдий",
	"Авель",
	"Авенир",
	"Аверий",
	"Аверкий",
	"Аверьян",
	"Авксентий",
	"Аврам",
	"Аврелиан",
	"Автоном",
	"Агап",
	"Агапий",
	"Агапит",
]

const city = [
  'Самара',
  'Новосибирск',
  'Калуга',
  'Клин',
  'Ижевск',
  'Москва',
  'Ростов',
  'Тула',
  'Краснодар',
  'Екатеринбург',
  'Томск',
  'Калуга',
]
const sex = [
  'мужчина',
  'женщина'
]
const status = [
  'с парой',
  'свободные',
]

function getTags() {
  return [
    city.sort(() => 0.5 - Math.random())[0],
    sex.sort(() => 0.5 - Math.random())[0],
    status.sort(() => 0.5 - Math.random())[0],
  ]
}

function ava(){
  return `https://randomuser.me/api/portraits/med/men/${ ~~(Math.random() * 50) }.jpg`
}
export const state = () => ({
  mentors: [ {
    name: "Дан Ёжик-Джедай" ,
    avatar: 'https://sun9-34.userapi.com/c851432/v851432152/ec95a/dBEMXXdq7Ig.jpg?ava=1',
    tags: [
      'мужчина',
      'Санкт-Петербург',
      'свободные',
    ],
    lenta: []
  } ,
    ...new Array(500).fill(0).map(( _, i) => ({
    name: names[~~(
      Math.random() * names.length
    )] ,
    avatar: ava(),
    tags: getTags(),
    lenta: new Array(10).fill(0).map((_,id) => ({
        // for line
        data: Date.now(),
        name: "test 2",
        icon: "eco",
        read: true,

        // for card
        type: "weather",
        id
      }))
     // mentors[0].lenta === store.lenta.mentor
  }))],
  ps: new Array(10).fill(0).map( _ => ({
    name: "ps name",
    avatar: "http://placehold.it/20x20"
  }))
})
