const baseUrl = "https://demo3.alpha.vkhackathon.com/api/v1";

export default class Api {
    constructor(ctx) {
        this.$axios = ctx.$axios;
        this.$store = ctx.$store;
        this.$router = ctx.$router;
    }

    async get(url) {
        var res = await this.$axios(`${baseUrl}/${url}`).catch(error => {
            return error.response.data;
        });
        if (res.code || res.data.error) {
            switch (res.code) {
                case 403:
                    this.$store.commit(
                        "error/show",
                        "Ваш токен устарел, требуется перезайти"
                    );
                    this.$router.push("/auth", true);
                    break;
                default:
                    this.$store.commit("error/show", `Ошибка:${res.message}`);
                    break;
            }
            if (res.data.error) {
                this.$store.commit("error/show", `Ошибка:${res.data.response}`);
            }
        }
        return res.data;
    }

    async post(url, data) {
        var res = await this.$axios.post(`${baseUrl}/${url}`, data).catch(error => {
            return error.response.data;
        });
        if (res.code || res.data.error) {
            switch (res.code) {
                case 403:
                    this.$store.commit(
                        "error/show",
                        "Ваш токен устарел, требуется перезайти"
                    );
                    this.$router.push("/login", true);
                    break;
                default:
                    this.$store.commit("error/show", `Ошибка:${res.message}`);
                    break;
            }
            if (res.data.error) {
                this.$store.commit("error/show", `Ошибка:${res.data.response}`);
            }
        }
        return res.data;
    }

    async getMe() {
        var res = await this.get(`user/me`);
        return res;
    }
    async setFcm(token) {
        var res = await this.post(`user/fcm`, { fcmToken: token });
        return res;
    }

    async pushGetGroupsReceivers() {
        var res = await this.get(`push/receivers/groups`);
        return res;
    }
    async pushGetUsersInGroupReceivers(groupId) {
        var res = await this.get(`push/receivers/users/${groupId}`);
        return res;
    }

    async pushSend(data) {
        var res = await this.post(`push/send`, JSON.parse(JSON.stringify(data)));
        return res;
    }

    async pushMy() {
        var res = await this.get(`push/my`);
        return res;
    }
    async pushView(pushId) {
        var res = await this.post(`push/view/${pushId}`);
        return res;
    }
    async pushReply(pushId, reply) {
        var res = await this.post(`push/reply/${pushId}`, { reply: reply });
        return res;
    }

    async vkProxy(method, url, body) {
        var res = await this.post(`public/vk-proxy`, { method, url, body });
        return res;
    }

    async authLogin(email, password) {
        var res = await this.post(`public/auth/login`, { email: email, password: password });
        return res;
    }
    async authRenew(renewToken) {
        var res = await this.post(`public/auth/renew`, { renewToken: renewToken });
        return res;
    }

    async knowledgeGet() {
        var res = await this.get(`knowledge/get`);
        return res;
    }
    async knowledgeById(id) {
        var res = await this.get(`knowledge/${id}`);
        return res;
    }

    async groupMy(id) {
        var res = await this.get(`group/my`);
        return res;
    }

    async chatBot(text) {
        var res = await this.post(`chat/bot`, {
            text: text
        });
        return res;
    }
}