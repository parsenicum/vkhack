const fs = require('fs')

const name = fs.readFileSync('./names.md', 'utf8').split('\n').filter(Boolean)

fs.writeFileSync('./names.md', JSON.stringify(
  name, null, '\t'
) , 'utf8')
