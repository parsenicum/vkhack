import Api from '@/api/index.js'

export default (ctx, inject) => {
    const api = new Api({ $axios: ctx.app.$axios, $store: ctx.store, $router: ctx.app.router })
    ctx.$api = api
    inject('api', api)
}